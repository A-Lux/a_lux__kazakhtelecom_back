<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\UserToRegion;
use Faker\Generator as Faker;

$factory->define(UserToRegion::class, function (Faker $faker) {
    return [
    	'user_id' => random_int(1, 10),
        'region_id' => random_int(1, 10)
    ];
});
