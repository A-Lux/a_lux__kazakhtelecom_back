<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Client;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Client::class, function (Faker $faker) {
    return [
        'user_id' => random_int(4, 10),
        'name' => $faker->name(),
        'address' => $faker->address(),
        'phone' => $faker->phoneNumber(),
        'email' => $faker->unique()->safeEmail,
        'company_id' => random_int(1, 10)
    ];
});
