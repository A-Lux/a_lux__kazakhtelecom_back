<?php

use App\Evacuator;
use Illuminate\Database\Seeder;

class EvacuatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Evacuator::class, 10)->create();
    }
}