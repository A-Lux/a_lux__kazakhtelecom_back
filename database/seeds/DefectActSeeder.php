<?php

use App\DefectAct;
use Illuminate\Database\Seeder;

class DefectActSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DefectAct::class, 10)->create();
    }
}