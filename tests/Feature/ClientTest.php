<?php

namespace Tests\Feature;

use App\Car;
use App\User;
use App\Driver;
use App\Company;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testClientCanRegister()
    {
        $company = Company::inRandomOrder()->first();
        $faker = \Faker\Factory::create();
        $data = [
            'name' => $faker->firstname,
            'email' => $faker->email,
            'company' => $company->id,
            'phone' => $faker->phoneNumber,
            'address' => $faker->streetAddress,
            'password' => 'secretsecret',
            'password_confirmation' => 'secretsecret',
            'agreement' => true
        ];

        $response = $this->json('POST', 'api/register/client', $data);
        return $response->assertStatus(200);
    }

    public function testClientCanMakeAnOrder() {
        $faker = \Faker\Factory::create();
        $user = User::where('role_id', 2)->first();
        $car = Car::inRandomOrder()->first();
        $driver = Driver::inRandomOrder()->first();
        $data = [
            'car_id' => $car->id,
            'is_evacuated' => 1,
            'location' => $faker->streetAddress,
            'driver_id' => $driver->id,
            'problem_description' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        ];
        $response = $this->actingAs($user, 'api')->json('POST', 'api/clients/orders/store', $data);
        return $response->assertStatus(200);
    }

    public function testClientCanGetOrders() {
        $user = User::where('role_id', 2)->first();
        $response = $this->actingAs($user, 'api')->json('GET', 'api/clients/orders');
        $response->assertSuccessful();
    }

    public function testClientCanGetCars() {
        $user = User::where('role_id', 2)->first();
        $response = $this->actingAs($user, 'api')->json('GET', 'api/clients/cars');
        $response->assertSuccessful();
    }
}
