<?php

namespace Tests\Feature;

use App\Car;
use App\User;
use App\Order;
use App\Client;
use App\Driver;
use App\Station;
use App\Evacuator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrpuTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testOrpuCanApplyAnOrder() {
        $user = User::where('role_id', 3)->first();
        $order = Order::where('status_internal', 1)->whereNull('orpu_id')->first();
        
        $response = $this->actingAs($user, 'api')->json('POST', 'api/orpus/orders/apply/'.$order->id);

        $response->assertSuccessful();
    }

    public function testOrpuCanStoreAEvacuator() {
        $faker = \Faker\Factory::create();
        $user = User::where('role_id', 3)->first();

        $data = [
            'name' => $faker->firstName,
            'phone' => $faker->phoneNumber
        ];


        $response = $this->actingAs($user, 'api')->json('POST', 'api/orpus/evacuators/store', $data);
        $response->assertSuccessful();
    }

    public function testOrpuCanStoreStation() {
        $faker = \Faker\Factory::create();
        $user = User::where('role_id', 3)->first();

        $data = [
            'name' => $faker->firstName,
            'city' => $faker->streetAddress,
            'address' => $faker->streetAddress,
            'fact_address' => $faker->streetAddress,
            'jur_address' => $faker->streetAddress,
            'bin' => $faker->isbn13,
            'bik' => $faker->isbn13,
            'iik' => $faker->isbn13,
            'contact_person' => $faker->firstName,
            'contact_phone' =>$faker->phoneNumber
        ];


        $response = $this->actingAs($user, 'api')->json('POST', 'api/orpus/stations/store', $data);
        $response->assertSuccessful();
    }
}
