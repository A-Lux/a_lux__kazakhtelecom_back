<?php

namespace App;

use App\Company;
use App\Traits\ReturnsFillables;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, ReturnsFillables, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'region_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'remember_token' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Get the Clients for the User.
     */
    public function client()
    {
        return $this->hasOne(\App\Client::class);
    }

    public function stationManager()
    {
        return $this->hasOne(\App\StationManager::class);
    }   

    /**
     * Get the Comments for the User.
     */
    public function comments()
    {
        return $this->hasMany(\App\Comment::class);
    }


    /**
     * Get the Role for the User.
     */
    public function role()
    {
        return $this->belongsTo(\App\Role::class);
    }

    public function region() {
        return $this->hasMany(\App\UserToRegion::class);
    }

    public function orders() {
        return $this->belongsToMany(\App\Order::class, 'user_order');
    }
}