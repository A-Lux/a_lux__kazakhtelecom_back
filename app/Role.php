<?php

namespace App;

use App\Traits\ReturnsFillables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use ReturnsFillables, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Get the Users for the Role.
     */
    public function users()
    {
        return $this->hasMany(\App\User::class);
    }


    /**
     * Get the Urls for the Role.
     */
    public function urls()
    {
        return $this->hasMany(\App\Url::class);
    }

}
