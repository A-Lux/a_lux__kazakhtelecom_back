<?php

namespace App;

use App\Traits\ReturnsFillables;
use App\Traits\ReturnsColumnsByRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use ReturnsFillables, SoftDeletes, ReturnsColumnsByRole;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'brand', 'model', 'company_id', 'year', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'number' => 'string',
        'brand' => 'string',
        'model' => 'string',
        'year' => 'integer',
        'type' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    static protected $columns = [
        'number' => ['ork'],
        'brand' => ['ork'],
        'model' => ['ork'],
        'year' => ['ork'],
        'type' => ['ork']
    ];

    /**
     * Get the Orders for the Car.
     */
    public function orders()
    {
        return $this->hasMany(\App\Order::class);
    }


    /**
     * Get the Company for the Car.
     */
    public function company()
    {
        return $this->belongsTo(\App\Company::class);
    }
}
