<?php

namespace App;

use App\Traits\ReturnsFillables;
use App\Traits\ReturnsColumnsByRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use ReturnsFillables, SoftDeletes, ReturnsColumnsByRole;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_id', 
        'client_id', 
        'is_evacuated', 
        'location', 
        'driver_id', 
        'problem_description', 
        'status', 
        'status_internal', 
        'ready_to_repair_at', 
        'contract_id', 
        'station_id', 
        'completed_at', 
        'evacuator_id', 
        'ork_id', 
        'orpu_id', 
        'orpa_id',
        'ready_to_diagnose_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_evacuated' => 'boolean',
        'location' => 'string',
        'problem_description' => 'string',
        'status' => 'integer',
        'status_internal' => 'integer',
        'ready_to_repair_at' => 'timestamp',
        'completed_at' => 'timestamp',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];
    static protected $columns = [
        'id' => ['ork', 'client'],
        'Описание проблемы' => ['ork', 'client'],
        'Номер договора' => ['ork', 'client'],
        'Заключен' => ['ork', 'client'],
        'Истекает' => ['ork', 'client'],
        'number' => ['ork', 'client'],
        'car_name' => ['ork'],
        'location' => ['ork', 'client'],
        'Требуется эвакуатор' => ['ork', 'client'],
        'station_name' => ['ork', 'client'],
        'Адрес СТО' => ['ork', 'client'],
        'телефон СТО' => ['client'],
        'status' => ['ork', 'client', 'orpa', 'orpu', 'station'],
        'evacuator_name' => ['ork'],
        'evacuator_phone' => ['ork'],
        'Дефектные акты' => ['client'],
        'created_at' => ['client'],
    ];
    /**
     * Get the DefectActs for the Order.
     */
    public function defectActs()
    {
        return $this->hasMany(\App\DefectAct::class);
    }


    /**
     * Get the Comments for the Order.
     */
    public function comments()
    {
        return $this->hasMany(\App\Comment::class);
    }


    /**
     * Get the Client for the Order.
     */
    public function client()
    {
        return $this->belongsTo(\App\Client::class);
    }


    /**
     * Get the Driver for the Order.
     */
    public function driver()
    {
        return $this->belongsTo(\App\Driver::class);
    }


    /**
     * Get the Car for the Order.
     */
    public function car()
    {
        return $this->belongsTo(\App\Car::class);
    }


    /**
     * Get the Evacuator for the Order.
     */
    public function evacuator()
    {
        return $this->belongsTo(\App\Evacuator::class);
    }


    /**
     * Get the Station for the Order.
     */
    public function station()
    {
        return $this->belongsTo(\App\Station::class);
    }


    /**
     * Get the Media for the Order.
     */
    public function media()
    {
        return $this->belongsToMany(\App\Media::class);
    }

    public function contract() {
        return $this->belongsTo(\App\Contract::class);
    }

    public function chatroom() {
        return $this->hasOne(\App\Chatroom::class);
    }

    public function users() {
        return $this->belongsToMany(\App\User::class,'user_order');
    }
}
