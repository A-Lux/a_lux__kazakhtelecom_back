<?php

namespace App\Http\Controllers;

use App\Url;
use App\Http\Resources\UrlCollection;
use App\Http\Resources\UrlResource;
 
class UrlAPIController extends Controller
{
    public function index()
    {
        return new UrlCollection(Url::paginate());
    }
 
    public function show(Url $url)
    {
        return new UrlResource($url->load(['role']));
    }

    public function store(Request $request)
    {
        return new UrlResource(Url::create($request->all()));
    }

    public function update(Request $request, Url $url)
    {
        $url->update($request->all());

        return new UrlResource($url);
    }

    public function destroy(Request $request, Url $url)
    {
        $url->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
