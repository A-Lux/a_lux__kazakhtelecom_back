<?php

namespace App\Http\Controllers;

use App\Part;
use App\Http\Resources\PartCollection;
use App\Http\Resources\PartResource;
 
class PartAPIController extends Controller
{
    public function index()
    {
        return new PartCollection(Part::paginate());
    }
 
    public function show(Part $part)
    {
        return new PartResource($part->load(['defectActs']));
    }

    public function store(Request $request)
    {
        return new PartResource(Part::create($request->all()));
    }

    public function update(Request $request, Part $part)
    {
        $part->update($request->all());

        return new PartResource($part);
    }

    public function destroy(Request $request, Part $part)
    {
        $part->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
