<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function unread(Request $request) {
        if($request->user()->role_id == 2) {
            $notifications = $request->user()->unreadNotifications->where('type', '!=','App\Notifications\CompanyHistory');
        }else {
            $notifications = $request->user()->unreadNotifications;
        }
        return response($notifications, 200);
    }

    public function read(Request $request) {
        if($request->user()->role_id == 2) {
            $notifications = $request->user()->readNotifications->where('type', '!=', 'App\Notifications\CompanyHistory');
        }else {
            $notifications = $request->user()->readNotifications;
        }
        return response($notifications, 200);
    }

    public function mark(Request $request) {
        $request
            ->user()
            ->unreadNotifications->markAsRead();
        
        return response(['message' => 'All notifications are marked as read'], 200);
    }

    public function markOne(Request $request, $notification_id) {
        $notification = $request
            ->user()
            ->unreadNotifications
            ->where('id', $notification_id)
            ->first();
        
        if($notification) {
            $notification->markAsRead();
        }

        return response(['message' => $notification->id + ' marked as read'], 200);
    }
}
