<?php

namespace App\Http\Controllers\Api\Client;

use App\User;
use App\Order;
use App\Driver;
use App\Status;
use App\Chatroom;
use App\UserOrder;
use App\Participant;
use App\UserToRegion;
use App\StationManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Templates\UserTemplate;
use App\Http\Resources\OrderResource;
use App\Http\Templates\OrderTemplate;
use App\Notifications\ClientRejectedAnOrder;
use App\Notifications\ClientAppliedDiagnoseDate;

class OrderController extends Controller
{
    public function show(Request $request, $id)
    {
        $order = $request
            ->user()
            ->orders()
            ->with(['client.company','driver','defectActs', 'defectActs.parts', 'defectActs.works', 'chatroom.participants.user', 'chatroom.messages.user', 'car'])
            ->where('orders.id', $id)
            ->first();
        
        $colNames = Order::getColumns('client');
        return response(['order' => $order, 'columns' => $colNames], 200);
    }

    public function getClientsOrders(Request $request, Order $order) {
        $orders = $request
            ->user()
            ->orders()
            ->select([
                'orders.id',
                'orders.problem_description',
                'contracts.number_of_сontract',
                'contracts.signed_at',
                'contracts.expire_at',
                'cars.number',
                'orders.location',
                'orders.is_evacuated',
                'stations.name as station_name', 
                'stations.address',
                'stations.contact_phone',
                'orders.status',
                'orders.created_at'
                ])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->leftJoin('stations', 'stations.id', 'orders.station_id')
            ->join('contracts', 'contracts.id', 'orders.contract_id');
        
        if($request->has('status_internal')) {
            $statuses = strpos($request->status_internal, ',') ? explode(',', $request->status_internal) : [$request->status_internal];
            $orders = $orders
                ->whereIn('status_internal', $statuses);
        }

        $orders = $orders->get();
        $colNames = Order::getColumns('client');
        if(isset($statuses)) {
            foreach($statuses as $status) {
                if($status >= 10) {
                    $colNames[] = Order::getColumn('defect_acts');
                    break;
                }
            }
        }

        return response(['orders' => $orders, 'columns' => $colNames], 200);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'car_id' => 'required|integer',
            'is_evacuated' => 'required',
            'location' => 'required|string',
            'contract_id' => 'required|integer',
            'problem_description' => 'required',
        ]);
        
        $company = $request->user()->client->company;
        $orkManagerId = UserTemplate::init()->getUser(1, $company->region_id)->id;
        $opManagerId = UserTemplate::init()->getUser(6, $company->region_id)->id;
        
        if($request->has('driver_id') && !is_null($request->driver_id)) {
            $driver_id = $request->driver_id;
        }else {
            $driver_id = Driver::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'company_id' => $company->id
            ])->id;
        }
    
        $status_id = Status::where('name', 'Новая заявка')->first()->id;
        $order = Order::create([
            'car_id' =>  $request->car_id,
            'is_evacuated' => $request->is_evacuated,
            'client_id' => $request->user()->client->id,
            'location' => $request->location,
            'actual_location' => $request->is_evacuated ? $request->actual_location : null,
            'contract_id' => $request->contract_id,
            'driver_id' => $driver_id,
            'status' => $status_id,
            'status_internal' => 0,
            'problem_description' => $request->problem_description
        ]);

        OrderTemplate::init($order)
            ->setUser($request->user()->id)
            ->setUser($orkManagerId)
            ->setUser($opManagerId);

        $chatroom = Chatroom::create([
            'order_id' => $order->id
        ]);

        Participant::create([
            'user_id' => $request->user()->id,
            'chatroom_id' => $chatroom->id
        ]);

        Participant::create([
            'user_id' => $orkManagerId,
            'chatroom_id' => $chatroom->id
        ]);
        
        return response(['message' => 'Order created'], 200);
    }

    public function applyDiagnoseAt(Order $order) {
        $client = $order->client;
        $company = $client->company;

        $stationManagerId = StationManager::where('station_id', $order->station_id)->first()->user_id;
        $stationManager = User::find($stationManagerId);
        $orkManager = $order->users()->where('role_id', 1)->first();
        $orpuManager = $order->users()->where('role_id', 3)->first();
        
        $orkManager->notify(new ClientAppliedDiagnoseDate($client->name, $company->name, $order->id));
        $orpuManager->notify(new ClientAppliedDiagnoseDate($client->name, $company->name, $order->id));
        $stationManager->notify(new ClientAppliedDiagnoseDate($client->name, $company->name, $order->id));
        // sockets

        $status_id = Status::where('name', 'Ведется диагностика')->first()->id;

        $order = OrderTemplate::init($order)
            ->changeStatus(5)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        return response($order, 200);
    }

    public function reject(Order $order) {
        $client = $order->client;
        $company = $client->company;

        $stationManagerId = StationManager::where('station_id', $order->station_id)->first()->user_id;
        $stationManager = User::find($stationManagerId);
        $orkManager = User::find($order->ork_id);
        $orpuManager = User::find($order->orpu_id);

        $status_id = Status::where('name', 'Заявка отменена')->first()->id;

        $order = OrderTemplate::init($order)
            ->changeStatus(12)
            ->changeStatus($status_id, false)
            ->getOrder();

        
        $orkManager->notify(new ClientRejectedAnOrder($client->name, $company->name, $order->id));
        $orpuManager->notify(new ClientRejectedAnOrder($client->name, $company->name, $order->id));
        $stationManager->notify(new ClientRejectedAnOrder($client->name, $company->name, $order->id));
        
        return response($order, 200);
    }
}
