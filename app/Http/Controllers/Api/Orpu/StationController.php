<?php

namespace App\Http\Controllers\Api\Orpu;

use App\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StationResource;
use App\Http\Resources\StationCollection;
 
class StationController extends Controller
{
    public function index()
    {
        return new StationCollection(Station::get());
    }
 
    public function show(Station $station)
    {
        return new StationResource($station->load(['orders', 'carTypes']));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'fact_address' => 'required|string',
            'jur_address' => 'required|string',
            'bin' => 'required|string',
            'bik' => 'required|string',
            'iik' => 'required|string',
            'contact_person' => 'required|string',
            'contact_phone' => 'required|string'
        ]);

        return response(new StationResource(Station::create($request->all())), 200);
    }

    public function update(Request $request, Station $station)
    {
        $station->update($request->all());

        return new StationResource($station);
    }

    public function destroy(Request $request, Station $station)
    {
        $station->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
