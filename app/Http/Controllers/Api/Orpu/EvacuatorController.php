<?php

namespace App\Http\Controllers\Api\Orpu;

use App\Evacuator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EvacuatorResource;
use App\Http\Resources\EvacuatorCollection;
 
class EvacuatorController extends Controller
{
    public function index()
    {
        return new EvacuatorCollection(Evacuator::get());
    }
 
    public function show(Evacuator $evacuator)
    {
        return new EvacuatorResource($evacuator->load(['orders']));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|string'
        ]);

        return response(new EvacuatorResource(Evacuator::create($request->all())), 200);
    }

    public function update(Request $request, Evacuator $evacuator)
    {
        $evacuator->update($request->all());

        return new EvacuatorResource($evacuator);
    }

    public function destroy(Request $request, Evacuator $evacuator)
    {
        $evacuator->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
