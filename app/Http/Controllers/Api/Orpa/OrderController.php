<?php

namespace App\Http\Controllers\Api\Orpa;

use App\Order;
use App\Status;
use App\DefectAct;
use App\DefectActInProcess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Templates\OrderTemplate;

class OrderController extends Controller
{
    public function show(Request $request, $id)
    {
        $order = $request
            ->user()
            ->orders()
            ->with(['client.company', 'defectActs', 'defectActs.parts', 'defectActs.works', 'chatroom.participants', 'chatroom.messages'])
            ->where('orders.id', $id)
            ->first();
        
        $colNames = ['id',
        'client_name',
        'car_number',
        'problem_description',
        'station_name',
        'address',
        'contact_person',
        'contact_phone',
        'evacuator_name',
        'evacuator_phone'];
        
        return response(['order' => $order, 'columns' => $colNames], 200);
    }

    public function getOrders(Request $request, Order $order) {
        $orders = $request
            ->user()
            ->orders()
            ->with('client.company')
            ->get();
        
        $colNames = ['id',
        'client_name',
        'car_number',
        'problem_description',
        'station_name',
        'address',
        'contact_person',
        'contact_phone',
        'evacuator_name',
        'evacuator_phone'];
        
        return response(['orders' => $orders, 'columns' => $colNames], 200);
    }

    public function approveDefectAct(Request $request, Order $order, DefectAct $defectAct) {
        $status_id = Status::where('name', 'ДА ожидает согласования в отделе по работе с партнерами')->first()->id;
        if(!DefectActInProcess::where('defect_act_id', $defectAct->id)
        ->where('order_id', $order->id)->exists()) {
            DefectActInProcess::create([
                'user_id' => $request->user()->id,
                'defect_act_id' => $defectAct->id,
                'order_id' => $order->id
            ]);
        }
        $order = OrderTemplate::init($order)
            ->changeStatus(7)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function attachFiles(Request $request, Order $order) {
        $this->validate($request, [
            'bill' => 'required'
        ]);
        
        $orpaManager = $order->orpa_id;
        OrderTemplate::init($order)
            ->attachBill($request->bill, $orpaManager, 2);
        
        return response('Files are stored', 200);
    }
}
