<?php

namespace App\Http\Controllers\Api\Ork;

use App\Client;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function index(Request $request, Company $company) {
        $columns = Client::getColumns('ork');
        $managers = Client::select(['clients.id', 'clients.name', 'clients.email', 'clients.phone'])
            ->where('company_id', $company->id)
            ->get();

        return response(['managers' => $managers, 'columns' => $columns], 200);
    }
}