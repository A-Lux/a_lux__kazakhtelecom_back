<?php

namespace App\Http\Controllers\Api\Ork;

use App\DefectAct;
use App\DefectPart;
use App\DefectWork;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefectActController extends Controller
{
    public function computeMarkup(Request $request, DefectAct $defectAct) {
        $this->validate($request, [
            'markup_percent' => 'required|integer|min:0'
        ]);
        
        $defectAct->fill($request->all())->save();

        $parts = DefectPart::where('defect_act_id', $defectAct->id)
            ->where('applied', 1)
            ->get();
        $works = DefectWork::where('defect_act_id', $defectAct->id)
            ->where('applied', 1)
            ->get();
        
        foreach($parts as $part) {
            $part->price_with_markup = $defectAct->markup_percent / 100 * $part->price + $part->price;
            $part->save();
        }
        foreach($works as $work) {
            $work->price_with_markup = $defectAct->markup_percent / 100 * $work->price + $work->price;
            $work->save();
        }

        return response(compact('defectAct', 'parts', 'works'), 200);
    }

    public function computeMarkupManually(Request $request, DefectAct $defectAct) {
        $this->validate($request, [
            'parts' => 'required',
            'works' => 'required'
        ]);
        
        foreach($request->parts as $part) {
            $p = DefectPart::find($part['id']);
            $p->price_with_markup = $part['price'];
            $p->save();
        }
        foreach($request->works as $work) {
            $w = DefectWork::find($work['id']);
            $w->price_with_markup = $work['price'];
            $w->save();
        }
        
        $parts = DefectPart::where('defect_act_id', $defectAct->id)
            ->get();
        $works = DefectWork::where('defect_act_id', $defectAct->id)
            ->get();
        
        return response(compact('defectAct', 'parts', 'works'), 200);
    }
}
