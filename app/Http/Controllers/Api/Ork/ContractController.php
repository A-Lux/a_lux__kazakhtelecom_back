<?php

namespace App\Http\Controllers\Api\Ork;

use App\Company;
use App\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\CompanyHistory;

class ContractController extends Controller
{
    public function index(Request $request, Company $company) {
        $contracts = Contract::select(['contracts.id', 'contracts.number_of_сontract', 'contracts.signed_at', 'contracts.expire_at', 'companies.name as company_name', 'contracts.created_at'])
            ->where('company_id', $company->id)
            ->join('companies', 'company_id', 'companies.id')
            ->get();
    
        $columns = Contract::getColumns('ork');

        return response(['contracts' => $contracts, 'columns' => $columns], 200);
    }

    public function show(Request $request, Company $company, Contract $contract) {
        $contract = Contract::where('company_id', $company->id)->where('id', $contract->id)->first();
        
        return response(['contract' => $contract, 'columns' => $columns], 200);
        return response($contract, 200);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'number_of_сontract' => 'required|unique:contracts',
            'signed_at' => 'required',
            'expire_at' => 'required'
        ]);
        $company = Company::find($request->company_id);
        $contract = Contract::create($request->all());
        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'contract',
                'action' => 'create',
                'id' => $contract->id
            ]));
        }
        return response($contract, 200);
    }

    public function update(Request $request, Contract $contract) {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'number_of_сontract' => 'required',
            'signed_at' => 'required',
            'expire_at' => 'required'
        ]);
        
        $contract->fill($request->all())->save();
        $company = Company::find($request->company_id);

        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'contract',
                'action' => 'edit',
                'id' => $contract->id
            ]));
        }
        return response($contract, 200);
    }
}
