<?php

namespace App\Http\Controllers\Api;

use App\Part;
use App\DefectAct;
use App\DefectPart;
use App\DefectWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\DefectActResource;
use App\Http\Resources\DefectActCollection;
 
class DefectActAPIController extends Controller
{
    public function index()
    {
        return new DefectActCollection(DefectAct::paginate());
    }
 
    public function show(DefectAct $defectAct)
    {
        return new DefectActResource($defectAct->load(['order', 'parts', 'works']));
    }

    public function update(Request $request, DefectAct $defectAct)
    {
        $defectAct->update($request->all());

        return new DefectActResource($defectAct);
    }

    public function destroy(Request $request, DefectAct $defectAct)
    {
        $defectAct->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
