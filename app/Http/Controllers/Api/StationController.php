<?php

namespace App\Http\Controllers\Api;

use App\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StationController extends Controller
{
    public function index() {
        $stations = Station::get();

        return response($stations, 200);
    }
}
