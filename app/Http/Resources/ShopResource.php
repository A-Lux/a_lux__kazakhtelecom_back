<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city' => $this->city,
            'address' => $this->address,
            'dir_name' => $this->dir_name,
            'dir_phone' => $this->dir_phone,
            'specialist_name' => $this->specialist_name,
            'specialist_phone' => $this->specialist_phone,
            'jur_address' => $this->jur_address,
            'fact_address' => $this->fact_address,
            'bin' => $this->bin,
            'bik' => $this->bik,
            'iik' => $this->iik,
            'contact_person' => $this->contact_person,
            'contact_phone' => $this->contact_phone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'car_types' => CarTypeResource::collection($this->whenLoaded('car_types'))
        ];
    }
}
