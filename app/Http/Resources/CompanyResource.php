<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'jur_address' => $this->jur_address,
            'fact_address' => $this->fact_address,
            'bin' => $this->bin,
            'bik' => $this->bik,
            'iik' => $this->iik,
            'contact_person' => $this->contact_person,
            'contact_phone' => $this->contact_phone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'clients' => ClientResource::collection($this->whenLoaded('clients')),
            'cars' => CarResource::collection($this->whenLoaded('cars'))
        ];
    }
}
