<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationManager extends Model
{
    protected $fillable = ['user_id', 'station_id'];
}
