<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ClientAppliedDefectAct extends Notification
{
    use Queueable;

    private $client_name;
    private $company_name;
    private $order_id;
    private $defect_act_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($client_name, $company_name, $order_id, $defect_act_id)
    {
        $this->client_name = $client_name;
        $this->company_name = $company_name;
        $this->order_id = $order_id;
        $this->defect_act_id = $defect_act_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'client_name' => $this->client_name,
            'company_name' => $this->company_name,
            'order_id' => $this->order_id,
            'defect_act_id' => $this->defect_act_id
        ];
    }
}
