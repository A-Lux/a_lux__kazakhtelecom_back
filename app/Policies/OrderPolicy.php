<?php

namespace App\Policies;

use App\User;
use App\Order;
use App\DefectAct;
use App\UserOrder;
use App\DefectPart;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, $order) {
        $order = Order::find($order);
        return UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
    }
    public function orderBelongsToClient(User $user, Order $order) {
        return $user->role_id == 2 && UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
    }
    public function store(User $user)
    {
        return $user->role_id == 2;
    }
    public function getClientOrders(User $user) {
        return $user->role_id == 2;
    }
    // Ork
    public function orderBelongsToOrk(User $user, Order $order) {
        return $user->role_id == 1 && UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
    }
    public function getOrkOrders(User $user) {
        return $user->role_id == 1;
    }
    // Orpu
    public function orderBelongsToOrpu(User $user, Order $order) {
        return $user->role_id == 3 && UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
    }
    public function getOrpuOrders(User $user) {
        return $user->role_id == 3;
    }
    public function applyOrpuOrder(User $user) {
        return $user->role_id == 3;
    }
    public function update(User $user, Order $order) {
        if($user->role_id == 3) {
            return UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
        }elseif($user->role_id == 5) {
            return $order->station_id == $user->stationManager->station_id;
        }
        return false;
    }
    // Station
    public function orderBelongsToStation(User $user, Order $order) {
        return $order->station_id == $user->stationManager->station_id;
    }
    public function attachFilesToOrder(User $user, $order) {
        if($user->role_id == 4 || $user->role_id == 5) {
            return $order->status_internal == 19;
        }
        return false;
    }
    // Orpa
    public function orderBelongsToOrpa(User $user, Order $order) {
        return $user->role_id == 4 && UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
    }

    public function getOrpaOrders(User $user) {
        return $user->role_id == 4;
    }
}
