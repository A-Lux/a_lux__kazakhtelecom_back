<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CarPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(User $user) {
        return $user->role_id == 1;
    }
    
    public function getClientsCars(User $user) {
        return $user->role_id == 2;
    }
}
