<?php
$router
    ->namespace('Station')
    ->prefix('stations')
    ->group(function() use ($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
                // Get order
                $router
                    ->get('{order}', 'OrderController@show')
                    ->middleware('can:show, App\Order,order');
                // Update Order
                $router
                    ->put('{order}', 'OrderController@update')
                    ->name('update.station.order');
                // Attach diagnose at
                $router
                    ->post('{order}/diagnose-at', 'OrderController@diagnoseAt')
                    ->name('station.diagnose_at.order')
                    ->middleware('can:orderBelongsToStation, App\Order,order');
                // Attach ready to reapait at
                $router
                    ->post('{order}/repair-at', 'OrderController@repairAt')
                    ->name('station.diagnose_at.order')
                    ->middleware('can:orderBelongsToStation, App\Order,order');
                // Stations orders
                $router
                    ->get('/', 'OrderController@getStationOrders')
                    ->name('get.stations.orders');
                // Attach deffect act
                $router
                    ->post('{order}/defect-acts', 'DefectActController@store')
                    ->name('store.defect.act')
                    ->middleware('can:store,App\DefectAct,order');
                // Update deffect act
                $router
                    ->put('{order}/defect-acts/{defectAct}', 'DefectActController@update')
                    ->name('store.defect.act')
                    ->middleware('can:update,App\DefectAct,defectAct,order');
                // Get deffect act
                $router
                    ->get('{order}/defect-acts/{defectAct}', 'DefectActController@show')
                    ->name('store.defect.act')
                    ->middleware('can:update,App\DefectAct,defectAct,order');
                // Approve deffect act
                $router
                    ->post('{order}/defect-acts/{defectAct}/approve', 'OrderController@approveDefectAct')
                    ->name('approve.defect.act')
                    ->middleware('can:defectActBelongsToStation, App\DefectAct,order,defectAct');
                // Start repair
                $router
                    ->post('{order}/start', 'OrderController@startRepair')
                    ->name('start.repair')
                    ->middleware('can:orderBelongsToStation, App\Order,order');
                // Release car
                $router
                    ->post('release', 'OrderController@releaseCar')
                    ->name('release.car')
                    ->middleware('can:releaseCar, App\Order');
                // Attach files
                $router
                    ->post('{order}/attach-files', 'OrderController@attachFiles')
                    ->name('attach.files')
                    ->middleware('can:attachFilesToOrder, App\Order,order');
            });
    });