<?php

use App\Part;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\Notifications\StationSetReadyToRepairAtDate;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::group(['middleware' => 'cors'], function() {
    Route::get('/ttt', function() {
        dd(User::find(1)->notifications->where('type', 'App\\Notifications\\StationSetReadyToRepairAtDate'));
    });
    /*
    * Snippet for a quick route reference
    */
    Route::get('/', function (Router $router) {
        return collect($router->getRoutes()->getRoutesByMethod()["GET"])->map(function($value, $key) {
            return url($key);
        })->values();   
    });
    Route::namespace('Api')
        ->group(function() {
            Route::resource('companies', 'CompanyAPIController', [
                'only' => ['index']
            ]);
            Route::resource('stations', 'StationController', [
                'only' => ['index']
            ]);
            Route::resource('shops', 'ShopAPIController', [
                'only' => ['index']
            ]);
        });
    // Route::resource('clients', 'ClientAPIController', [
    //     'only' => ['index', 'show', 'store', 'update', 'destroy']
    // ]);


    // Route::resource('orders', 'OrderAPIController', [
    //     'only' => ['index', 'show', 'store', 'update', 'destroy']
    // ]);

    Route::resource('drivers', 'DriverAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('roles', 'RoleAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('urls', 'UrlAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('cars', 'CarAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('evacuators', 'EvacuatorAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('defectActs', 'DefectActAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('parts', 'PartAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('carTypes', 'CarTypeAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);


    Route::resource('media', 'MediaAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('works', 'WorkAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('comments', 'CommentAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    Route::resource('users', 'UserAPIController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);

    // Login
    Route::namespace('Api\Auth')
        ->group(function($router) {
            $router
                ->prefix('password')
                ->group(function() use ($router) {
                    $router
                        ->post('create', 'PasswordResetController@create');
                    $router
                        ->get('find/{token}', 'PasswordResetController@find');
                    $router
                        ->post('reset', 'PasswordResetController@reset');
                });
            
            $router
                ->prefix('register')
                ->group(function() use ($router) {
                    // Client registration
                    $router
                        ->post('client', 'RegisterController@registerClient')
                        ->name('register.client');
                    // Station registration
                    $router
                        ->post('station', 'RegisterController@registerStation')
                        ->name('register.station');
                });

            // Login
            $router
                ->post('login', 'LoginController@login')
                ->name('login');
        });

    Route::middleware('auth:api')
        ->namespace('Api')
        ->group(function ($router) {
            $router
                ->post('/auth/check', 'AuthCheckController@check')
                ->name('auth.check');

            $router->get('/sds', function() {
                return 200;
            });

            require('clients.php'); // 2
            require('orks.php'); // 1
            require('orpus.php'); // 3
            require('orpas.php'); // 4
            require('stations.php'); // 5
            require('ops.php'); //6
            // Logout
            $router
                ->post('/logout', 'Auth\LogoutController@logout')
                ->name('logout');
            
            $router
                ->get('/notifications/unread', 'NotificationController@unread');
            $router
                ->get('/notifications/read', 'NotificationController@read');
            $router
                ->post('/notifications/mark', 'NotificationController@mark');
            $router
                ->post('/notifications/mark/{notification}', 'NotificationController@markOne');
        });

    Route::get('fillables/{model}', function($model) {
            $model = "App\\".$model;
            $model = new $model();
            return response($model->getFillables());
        });

    Route::get('/entities/list', function(){
        $path = app_path();
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') continue;
            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                
            }else{
                $out[] = substr($result,0,-4);
            }
        }
        dd(1);
        return response($out);
    });

// });