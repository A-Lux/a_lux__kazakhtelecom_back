<?php
$router
    ->namespace('Orpu')
    ->prefix('orpus')
    ->group(function() use ($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
                // Get ORPU orders
                $router
                    ->get('/', 'OrderController@getOrders')
                    ->name('get.orpu.orders')
                    ->middleware('can:getOrpuOrders, App\Order');
                // Get order
                $router
                    ->get('{order}', 'OrderController@show')
                    ->middleware('can:show, App\Order,order');
                // Update Order
                $router
                    ->put('{order}', 'OrderController@update')
                    ->name('update.orpu.order')
                    ->middleware('can:orderBelongsToOrpu,App\Order,order');
                // Approve deffect act
                $router
                    ->post('{order}/approve', 'OrderController@approveDefectAct')
                    ->name('approveOrpa.defect.act')
                    ->middleware('can:orderBelongsToOrpu,App\Order,order');
                // Order rejected
                $router
                    ->post('{order}/reject', 'OrderController@reject')
                    ->name('reject.order')
                    ->middleware('can:orderBelongsToOrpu, App\Order,order');
                // Send to ORPA
                $router
                    ->post('{order}/send-to-orpa', 'OrderController@sendToOrpa');
            });
        // Store evacuator
        $router
            ->post('evacuators/store', 'EvacuatorController@store')
            ->name('orpu.attach.evacuator')
            ->middleware('can:store, App\Evacuator');
        $router
            ->get('evacuators', 'EvacuatorController@index');
        $router
            ->get('evacuators/{evacuator}', 'EvacuatorController@show');
        // Store station
        $router
            ->post('stations/store', 'StationController@store')
            ->name('orpu.attach.evacuator')
            ->middleware('can:store, App\Station');
        $router
            ->get('stations', 'StationController@index');
        $router
            ->get('stations/{station}', 'StationController@station');
        // Start repair
        // $router
        //     ->post('orders/{order}/repair', 'OrderController@repair')
        //     ->name('repair.status')
        //     ->middleware('can:sendRepair, order, App\Order');
    });