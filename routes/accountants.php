<?php 
$router
    ->namespace('Accountant')
    ->prefix('accountants')
    ->group(function() use($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
                // Accountants's orders
                $router
                    ->get('/', 'OrderController@index');
                    // middleware
                $router
                    ->get('{order}', 'OrderController@show');
                    // middleware
                // Bill is paid
                $router
                    ->post('{order}/defect-acts/{defectAct}/paid', 'OrderController@paid');
            });
    });