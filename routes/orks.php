<?php
$router
    ->namespace('Ork')
    ->prefix('orks')
    ->group(function() use ($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
                // Ork sends order to ORPU
                $router
                    ->post('repair/{order}', 'OrderController@onRepair')
                    ->middleware('can:orderBelongsToOrk,App\Order,order');
                // Ork sends order to ORPA if repair is not required
                $router
                    ->post('buy-parts-only/{order}', 'OrderController@buyParts')
                    ->middleware('can:orderBelongsToOrk,App\Order,order');
                // Ork orders
                $router
                    ->get('/', 'OrderController@getOrders')
                    ->middleware('can:getOrkOrders,App\Order');
                // Get order
                $router
                    ->get('{order}', 'OrderController@show')
                    ->middleware('can:show, App\Order,order');
                // Approve defect act
                $router
                    ->post('{order}/approve', 'OrderController@approveDefectAct')
                    ->middleware('can:orderBelongsToOrk,App\Order,order');
                // Reject diagnose_at
                $router
                    ->post('{order}/reject/diagnose_at', 'OrderController@rejectDiagnoseAt')  
                    ->middleware('can:orderBelongsToOrk,App\Order,order');
            });
        // Regions
        $router
            ->get('regions', 'RegionController@index');
        //Companies
        $router
            ->apiResource('companies', 'CompanyController');
        // Create a contract
        $router
            ->post('contracts', 'ContractController@store')
            ->middleware('can:store, App\Contract');
        // Get all contracts
        $router
            ->get('companies/{company}/contracts', 'ContractController@index');
        // Get one contract
        $router
            ->get('companies/{company}/contracts/{contract}', 'ContractController@show');
        // Update contract
        $router
            ->put('contracts/{contract}', 'ContractController@update');
        // Create driver
        $router
            ->post('drivers', 'DriverController@store')
            ->middleware('can:store, App\Driver');
        $router
            ->put('drivers/{driver}', 'DriverController@update');
        // Get all drivers
        $router
            ->get('companies/{company}/drivers', 'DriverController@index');
        // Get one driver
        $router
            ->get('companies/{company}/drivers/{driver}', 'DriverController@show');
        // Create car
        $router
            ->post('cars', 'CarController@store')
            ->middleware('can:store, App\Car');
        // Get all cars
        $router
            ->get('companies/{company}/cars', 'CarController@index');
        // Get one car
        $router
            ->get('companies/{company}/cars/{car}', 'CarController@show');
        $router
            ->put('cars/{car}', 'CarController@update');
        // Get Managers
        $router
            ->get('companies/{company}/managers', 'ClientController@index');
        // Apply part from defect act
        $router
            ->post('defect-acts/parts/{defectPart}/apply', 'DefectPartController@apply')
            ->middleware('can:defectPartBelongsToOrk,App\DefectAct,defectPart');
        // Update part from defect act
        $router
            ->put('defect-acts/parts/{defectPart}', 'DefectPartController@update')
            ->middleware('can:defectPartBelongsToOrk,App\DefectAct,defectPart');
        // Apply work from defect act
        $router
            ->post('defect-acts/works/{defectWork}/apply', 'DefectWorkController@apply')
            ->middleware('can:defectWorkBelongsToOrk,App\DefectAct,defectWork');
        // Update part from defect act
        $router
            ->put('defect-acts/works/{defectWork}', 'DefectWorkController@update')
            ->middleware('can:defectWorkBelongsToOrk,App\DefectAct,defectWork');
        // Compute markup
        $router
            ->post('defect-acts/compute-markup/{defectAct}', 'DefectActController@computeMarkup')
            ->middleware('can:defectActBelongsToOrk,App\DefectAct,defectAct');
        // Change markup manually
        $router
            ->post('defect-acts/compute-markup-manually/{defectAct}', 'DefectActController@computeMarkupManually')
            ->middleware('can:defectActBelongsToOrk,App\DefectAct,defectAct');
    });